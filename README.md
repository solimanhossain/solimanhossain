### Hi there!👋

It's me [Soliman Hossain]() :snowman:<br>
Software Engineer | Developer ([React](https://react.dev/) | [Next.js](https://nextjs.org/))<br>
Studied at Jahangirnagar University ([IIT](https://www.juniv.edu/institute/iit))<br><br>

<!-- <table>
  <tr>
    <th>
     <a href="https://www.linkedin.com/in/solimanhossain/"><img alt="Twitter" title="Linkedin" height="42" width="42" src="https://raw.githubusercontent.com/solimanhossain/solimanhossain/e98b19b4237f5d64336ce0d8568f4e11fe7c8082/images/linkedin.svg"></a>
    </th>
    <th>
     <a href="https://github.com/solimanhossain/solimanhossain.github.io/raw/main/content/cv.pdf"><img alt="CV" title="Resume" height="42" width="42" src="https://raw.githubusercontent.com/solimanhossain/solimanhossain/e98b19b4237f5d64336ce0d8568f4e11fe7c8082/images/cv.svg"></a>
    </th>
    <th>
     <a href="https://solimanhossain.github.io/"><img alt="myweb" src="https://raw.githubusercontent.com/solimanhossain/solimanhossain/e98b19b4237f5d64336ce0d8568f4e11fe7c8082/images/web.svg" title="Portfolio" width="42" height="42" /></a>
    </th>
    <th>
     <a href="https://leetcode.com/solimanhossain/"><img alt="Leetcode" title="Leetcode" height="42" width="42" src="https://raw.githubusercontent.com/solimanhossain/solimanhossain/e98b19b4237f5d64336ce0d8568f4e11fe7c8082/images/leetcode.svg"></a>
    </th>
  </tr>
</table> -->

> <img src="https://github.com/solimanhossain/solimanhossain/raw/main/icons/email.svg" width="20"> solimanhossainsohan@gmail.com <br><br> 
> [![Telegram](https://img.shields.io/badge/telegram-2CA5E0?logo=telegram&logoColor=white)](https://t.me/solimanhossain) 
> [![Unsplash](https://img.shields.io/badge/Pixabay-2EC66D.svg?logo=Pixabay&logoColor=white)](https://pixabay.com/users/solimanhossain) <br><br>

---

https://github.com/solimanhossain?tab=repositories

https://gitlab.com/users/solimanhossain/projects